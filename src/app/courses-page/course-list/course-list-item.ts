export interface CourseListItem {
  id: string;
  title: string;
  creationDate: Date;
  duration: number;
  description: string;
}
